package dev.codenmore.game.entities.creatures;

import java.awt.Color;
import java.awt.Graphics;

import dev.codenmore.game.Game;
import dev.codenmore.game.Handler;
import dev.codenmore.game.gfx.Animation;
import dev.codenmore.game.gfx.Assets;

public class Player extends Creature {
	
	// Animations
	private Animation animDown;

	public Player(Handler handler, float x, float y) {
		super(handler, x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
		
		// Collision size
		bounds.x = 16;
		bounds.y = 32;
		bounds.width = 32;
		bounds.height = 32;
		
		// Animations
		animDown = new Animation(500, Assets.player_down);
	}

	@Override
	public void tick() {
		// Animations
		animDown.tick();
		// Movement
		getInput();
		move();
		handler.getGameCamera().centerOnEntity(this);
	}
	
	private void getInput() {
		xMove = 0;
		yMove = 0;
		
		if(handler.getKeymanager().up)
			yMove = -speed;
		if(handler.getKeymanager().down)
			yMove = speed;
		if(handler.getKeymanager().left)
			xMove = -speed;
		if(handler.getKeymanager().right)
			xMove = speed;
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(animDown.getCurrentFrame(), (int) (x - handler.getGameCamera().getxOffset()), (int) (y - handler.getGameCamera().getyOffset()), width, height, null);
		
	}

}
