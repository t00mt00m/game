package dev.codenmore.game.states;

import java.awt.Graphics;

import dev.codenmore.game.Game;
import dev.codenmore.game.Handler;
import dev.codenmore.game.entities.creatures.Player;
import dev.codenmore.game.gfx.Assets;
import dev.codenmore.game.tiles.Tile;
import dev.codenmore.game.world.World;

public class GameState extends State {
	
	private Player player;
	private World world;
	
	public GameState(Handler handler) {
		super(handler);
		world = new World(handler, "res/worlds/world1.txt");
		handler.setWorld(world);
		player = new Player(handler, 100, 100);
		
	}
	
	@Override
	public void tick() {
		world.tick();
		player.tick();
	}

	@Override
	public void render(Graphics g) {
		world.render(g);
		player.render(g);
	}
	
}


