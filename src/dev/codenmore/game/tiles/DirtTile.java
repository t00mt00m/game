package dev.codenmore.game.tiles;

import dev.codenmore.game.gfx.Assets;

public class DirtTile extends Tile {

	public DirtTile(int id) {
		super(Assets.dirt, id);
	}

}
