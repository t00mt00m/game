package dev.codenmore.game;

import dev.codenmore.game.display.Display;

public class Launcher {

	public static void main(String[] args) {
		Game game = new Game("Game Title", 600, 600);
		game.start();
	}
	
}
