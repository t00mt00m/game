package dev.codenmore.game;

import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.Graphics;

import dev.codenmore.game.display.Display;
import dev.codenmore.game.gfx.Assets;
import dev.codenmore.game.gfx.ImageLoader;
import dev.codenmore.game.gfx.SpriteSheet;

public class Game implements Runnable {
	
	private Display display;
	public int width, height;
	public String title;
	
	private boolean running = false;
	private Thread thread;
	
	private BufferStrategy bs;
	private Graphics g;
	
	
	public Game(String title, int width, int height) {
		this.width = width;
		this.height = height;
		this.title = title;
	}
	
	private void init() {
		display = new Display(title, width, height);
		Assets.init();
	}
	
	private void tick() {
		
	}
	
	private void render() {
		bs = display.getCanvas().getBufferStrategy();
		if(bs == null) {
			display.getCanvas().createBufferStrategy(3);
			return;
		}
		g = bs.getDrawGraphics();
		//Clear screen
		g.clearRect(0, 0, width, height);
		
		g.drawImage(Assets.grass, 10, 10, null);
		
		bs.show();
		g.dispose();
		
	}
	
	public void run() {
		
		init();
		
		while(running) {
			tick();
			render();
		}
		
		stop();
		
	}
	
	public synchronized void start() {
		if(running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public synchronized void stop() {
		if(!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
